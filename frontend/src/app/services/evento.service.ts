import { Injectable } from '@angular/core';
import { MensajeService } from './mensaje.service';
import { Evento } from '../api/model/evento';
import { EventoControllerService } from '../api';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})

export class EventoService {
  public eventoProvider = new BehaviorSubject<Evento>(null);
  public evento = this.eventoProvider.asObservable();
  public eventosProvider = new BehaviorSubject<Evento[]>([]);
  public eventos = this.eventosProvider.asObservable();
  public user = null; 

  filter: any;
  
  
  constructor(
    private mensajeService: MensajeService,
    public userService:UserService, 
    private eventoService: EventoControllerService,
    private router: Router

  ) {
    this.userService.user.subscribe(user => {
      this.user = user;
      this.updateEventos();
    });

  }

  updateEventos(): void {
    this.filter = "{\"include\": [{\"relation\": \"user\"}]}";
    if (this.userService.authenticated()) {
      if(this.user.rol !== "admin" ){
        this.filter = "{\"where\": {\"userId\":\""+this.user.id+"\"},\"include\": [{\"relation\": \"user\"}]}";
        this.eventoService.eventoControllerFind(this.filter).subscribe((eventos) => {
          this.eventosProvider.next(eventos);
        })
      }else{
        this.eventoService.eventoControllerFind(this.filter).subscribe((eventos) => {
          this.eventosProvider.next(eventos);
        })
      }
    }else{
    }

  }
  updateEventosAlways(): void {
    this.filter = "{\"include\": [{\"relation\": \"user\"}]}";
    this.eventoService.eventoControllerFind(this.filter).subscribe((eventos) => {
      this.eventosProvider.next(eventos);
    })
  }


  getEventos(): Observable<Evento[]> {
    return this.eventos;
  }

  deleteEvento(id: string): void {
    const idDate = id;
    this.eventoService.eventoControllerDeleteById(idDate)
    .subscribe((data: any)=>{
      this.eventos.pipe(
        map(eventos => eventos.filter(evento => evento.id != id))
      ).subscribe(events => {
        this.eventosProvider.next(events);
      }) 
      this.mensajeService.open('el evento #'+idDate+' se elimino correctamente');
    });
  }

  createEvento(evento): void {
    this.eventoService.eventoControllerCreate(evento)
    .subscribe((evento) => {
      this.eventos.pipe(take(1)).subscribe(eventos => {
        const newArr = [...eventos, evento];
        this.eventosProvider.next(newArr);
      })
      this.mensajeService.open('el evento #'+evento.titulo+' se creo correctamente');
      this.router.navigate(['/evento/'+evento.id+'/opciones']);
    });
  }

  updateEvento(evento: Evento): void {

    this.eventoService.eventoControllerUpdateById(evento.id, evento)
    .subscribe((events) => {
      this.eventos.pipe(take(1)).subscribe(eventos => {
        let updateItem = eventos.find(event => event.id === evento.id);
        let index = eventos.indexOf(updateItem);   
        eventos[index] = evento;
        this.eventosProvider.next(eventos);
      })
      this.mensajeService.open('el evento #'+evento.titulo+' se actualizo correctamente');
      this.router.navigate(['/evento/'+evento.id+'/opciones']);
    });
  }

  findIndexToUpdate(evento) { 
      return evento.id === this;
  }


  getEvento():Observable<Evento>{
      return this.evento;
  }

  getOneEvento(id){
    this.filter = "{\"include\": [{\"relation\": \"user\"}]}";
    this.eventoService.eventoControllerFindById(id, this.filter).subscribe((evento) => {
      this.eventoProvider.next(evento);
    })
  }
  
  selectEvento(id):Observable<Evento>{
    let eventoFilter = this.eventos.pipe(map(eventos => eventos.find(e => e.id === id)));
    eventoFilter.subscribe(event => {
      if (event) {
        this.eventoProvider.next(event)
      }else{
        this.getOneEvento(id);
      }
    });
    return this.getEvento();
  }


}
