import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { MensajeService } from './services/mensaje.service';

import { UserService } from './services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private auth: UserService,
    private mensajeService: MensajeService,
    ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):boolean{
      if (this.auth.authenticated()) {
        return true;
      }
      this.mensajeService.open('No tienes permiso para ver esta página.');
      return false;
  }
}