import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventoComponent } from './components/evento/evento.component';
import { CrearEventoComponent } from './components/evento/add-evento.component';
import { EditEventoComponent } from './components/evento/edit-evento.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { OpcionesComponent } from './components/opciones/opciones.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './auth.guard';
import { VotacionComponent } from './components/votacion/votacion.component';
import { PreVotacionComponent } from './components/votacion/pre-votacion.component';
import { GraciasComponent } from './components/gracias/gracias.component';

const routes: Routes = [
  { path:'', redirectTo:'/inicio', pathMatch:'full'},
  { path:'dashboard', component:DashboardComponent, canActivate: [AuthGuard] , pathMatch: 'full'},
  { path:'evento/:id/edit', component:EditEventoComponent, canActivate: [AuthGuard] , pathMatch: 'full'},
  { path:'evento/:id/delete', component:EventoComponent, canActivate: [AuthGuard] , pathMatch: 'full'},
  { path:'evento/crear', component:CrearEventoComponent, pathMatch: 'full' },
  { path:'evento/:id', component:EventoComponent , pathMatch: 'full'},
  { path:'evento/:id/opciones', component:OpcionesComponent, pathMatch: 'full' },
  { path:'eleccion/:id', component:PreVotacionComponent, pathMatch: 'full' },
  { path:'eleccion/:id/opciones', component:VotacionComponent, pathMatch: 'full'},
  { path:'gracias', component:GraciasComponent, pathMatch: 'full' },
  { path:'inicio', component:InicioComponent , pathMatch: 'full'},
  { path: 'login', component: LogInComponent },
  { path: 'register', component: RegisterComponent },
  { path:'**', component:PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
