import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/api/model/user';
import { UserService } from '../../services/user.service';
// import { UserControllerService } from '../api';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  users: User[];
  displayedColumns = ['id', 'email', 'realm'];
  isAdmin:boolean = false;
  constructor(
    private userService:UserService, 
  ) { 
    if (this.userService.authenticated()) {
      this.userService.user.subscribe(user => {
        if (user.rol == "admin") {
          this.isAdmin = true;
          this.userService.updateUsers();
          this.userService.users.subscribe(users => {
            this.users = users;
          });
        }
      });

    }
  }

  ngOnInit(): void {
  }
  
  delete(id: string): void {
    const idDate = id;
  }

}
