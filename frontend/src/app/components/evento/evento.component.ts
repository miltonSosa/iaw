import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Option } from '../../api/model/option';
import { EventoService } from '../../services/evento.service'
import { Evento } from '../../api/model/evento'
import { OpcionService } from 'src/app/services/opcion.service';
import {Clipboard} from '@angular/cdk/clipboard'; 
import { MensajeService } from 'src/app/services/mensaje.service';
import { OptionVotesControllerService } from '../../api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})

export class EventoComponent implements OnInit {

  evento: Evento;
  @Input('cdkCopyToClipboard') link:string;
  opciones: Option[];
  selectedOption:Option = null;

  dias: {[key: number]: string} = {
    0:'Lunes',
    1: 'Martes',
    2: 'Miercoles',
    3: 'Jueves',
    4: 'Viernes',
    5: 'Sabado',
    6: 'Domingo'
  };

  votes: { value: number, cant:number, name: string, icon:string, color:string}[] = [
    { value:1, cant: 0, name: "Si", icon:"gpp_good", color:"#248600"},
    { value:2, cant: 0, name: "Puede ser", icon:"gpp_maybe", color:"#865e00"},
    { value:3, cant: 0, name: "No", icon:"gpp_bad", color:"#a40000"}
  ];

  displayedColumns: string[] = ['valor', 'cantidad'];
  
  constructor(
    private route: ActivatedRoute,
    private eventoService: EventoService,
    private opcionService:OpcionService,
    private router: Router,
    private mensajeService: MensajeService,     
    private clipboard: Clipboard,
    private location: Location
    ) {}
    
    ngOnInit(): void {
      const id = this.route.snapshot.paramMap.get('id');
      this.link = 'http:localhost:4200'+this.router.createUrlTree(['/eleccion', id]).toString();
      this.opcionService.updateDataOpcionesFromEvento(id);
      this.eventoService.selectEvento(id);
      this.eventoService.getEvento().subscribe(evento => {
        this.evento = evento;
      });
      this.opcionService.options.subscribe(opciones => {
        this.opciones = opciones;
        if (this.opciones){
          this.opciones.forEach((opcion)=>{
            this.votes.forEach((vote)=>{
              if (opcion.votes) {
                vote.cant = (opcion.votes.filter((s) => s.valor ===vote.value )).length;
              }else{
                vote.cant = 0 ;
              }
            });
            opcion.data=this.votes;
          });
        }
      });
    }

    getLink(){
      this.clipboard.copy(this.link);
      this.mensajeService.open('El link se copio correctamente, ya lo podes compartir!');
    }
    
    goBack(): void {
      this.location.back();
    }
    
    onNgModelChange(data){
      this.selectedOption.dia=data[0].dia
      this.votes.forEach((vote)=>{
        if (data[0].votes) {
          vote.cant = data[0].votes.filter((s) => s.valor ===vote.value ).length;
        }else{
          vote.cant = 0;
        }
      });
      this.selectedOption.votes=this.votes;
    }
}