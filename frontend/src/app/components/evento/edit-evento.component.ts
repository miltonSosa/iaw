import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Evento } from '../../api/model/evento';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventoService } from '../../services/evento.service';
import { UserService } from '../../services/user.service';
import { User } from 'src/app/api';

@Component({
  selector: 'app-edit-evento',
  templateUrl: './edit-evento.component.html',
  styleUrls: ['./evento.component.css']
})

export class EditEventoComponent implements OnInit {
  public form: FormGroup;
  public evento: Evento;
  public user: User;
    
  constructor(
    private route: ActivatedRoute,
    public eventoService: EventoService,
    private location: Location,
    public userService:UserService, 
    private formBuilder: FormBuilder
    ) {
    }
    
    ngOnInit(): void {
      const id = this.route.snapshot.paramMap.get('id');
      this.eventoService.selectEvento(id);
      this.eventoService.getEvento().subscribe(evento => {
        this.evento = evento;
      });

      if (this.userService.authenticated()) {
        this.userService.user.subscribe(user => {
          this.user = user;
        });
      }
      this.form = this.formBuilder.group({
          titulo: ['', Validators.required],
          descripcion: ['', Validators.required],
          password: ['password', Validators.required],
          desde: [new Date(), Validators.required],
          email: ['', [Validators.required, Validators.email]],
          duracion: ['', Validators.required],
          hasta: [new Date(), Validators.required],
          user: [''],
      });
    }
    
    onSubmit() {
      this.evento = this.form.value;
      this.evento.user = this.evento.user || this.user.id;
      const id = this.route.snapshot.paramMap.get('id');
      this.evento.id = id;
      this.evento.email = this.evento.email || this.user.email;
      this.eventoService.updateEvento(this.evento);
    }

    onReset() {
      this.form.reset();
    }

    goBack(): void {
      this.location.back();
    }
    
}
