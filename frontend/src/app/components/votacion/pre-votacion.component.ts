import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Evento } from 'src/app/api';
import { EventoService } from 'src/app/services/evento.service';
import { MensajeService } from './../../services/mensaje.service';

@Component({
  selector: 'app-pre-votacion',
  templateUrl: './pre-votacion.component.html',
  styleUrls: ['./votacion.component.css']
})
export class PreVotacionComponent implements OnInit {
  userForm: FormGroup;
  evento: Evento;

  constructor(
    public eventoService: EventoService,
    private formBuilder: FormBuilder,
    private mensajeService: MensajeService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    const id = this.route.snapshot.paramMap.get('id');
    this.eventoService.getOneEvento(id);
    this.eventoService.getEvento().subscribe(evento => {
      this.evento = evento;
      if (evento && evento.password === null ) {
        this.router.navigate(['/eleccion/'+id+'/opciones']);
      }
    });
    this.userForm = this.formBuilder.group({
      password: ['', Validators.required],
    }); 

   
  }

  ngOnInit(): void {
    // const id = this.route.snapshot.paramMap.get('id');
    // this.eventoService.selectEvento(id);

    // this.userForm = this.formBuilder.group({
    //   password: ['', Validators.required],
    // }); 
    

    // this.eventoService.getEvento().subscribe(evento => {
    //   this.evento = evento;
    //   if (evento && evento.password === null ) {
    //     this.router.navigate(['/eleccion/'+id+'/opciones']);
    //   }
    // });
  }
  
  onSubmit(){
    if (this.userForm.invalid) {
      return false;
    }
    if ( this.userForm.value.password === this.evento.password){
      this.router.navigate(['/eleccion/'+this.evento.id+'/opciones']);
    } else {
      this.mensajeService.open('la contraseña ingresada es incorrecta');
    }

  }

}