import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  userForm: FormGroup;

  constructor(
    private userService:UserService, 
    private formBuilder: FormBuilder
  ) {}


  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      password: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
    }); 
  }

  onSubmit(){
    if (this.userForm.invalid) {
      return false;
    }
    this.userService.login(this.userForm.value);

  }

}
