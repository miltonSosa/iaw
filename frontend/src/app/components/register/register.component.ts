import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userForm: FormGroup;

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      realm: ['', Validators.required],
      password: ['', [Validators.required, Validators.min(6)]],
      email: ['', [Validators.required, Validators.email]],
      rol: ['user']
    }); 
  }

  constructor(
    private userService:UserService, 
    private formBuilder: FormBuilder
  ) {}

  onSubmit(){
    if (this.userForm.invalid) {
      return false;
    }
    this.userService.signUp(this.userForm.value);
  }

}
