import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Option } from '../../api/model/option';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators,FormControl, FormArray } from '@angular/forms';
import { OpcionService } from '../../services/opcion.service';
import { Evento } from '../../api/model/evento';
import { EventoService } from '../../services/evento.service';

@Component({
  selector: 'app-opciones',
  templateUrl: './opciones.component.html',
  styleUrls: ['./opciones.component.css']
})
export class OpcionesComponent implements OnInit {

  form: FormGroup;
  eventoId: string;
  evento: Evento;
  listOpciones: Option[];
  // public opcion: Option;

  dias= [
    { id: 0, name: 'Lunes' },
    { id: 1, name: 'Martes' },
    { id: 2, name: 'Miercoles' },
    { id: 3, name: 'Jueves' },
    { id: 4, name: 'Viernes' },
    { id: 5, name: 'Sabado' },
    { id: 6, name: 'Domingo' }
  ];


  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private opcionService:OpcionService, 
    public eventoService: EventoService,
    private formBuilder: FormBuilder
  ) { 
    this.eventoService.updateEventos();

  }

  ngOnInit(): void {
    this.eventoId = this.route.snapshot.paramMap.get('id');
    // pido evento
    this.eventoService.selectEvento(this.eventoId);
    this.opcionService.updateDataOpcionesFromEvento(this.eventoId);
    this.eventoService.getEvento().subscribe(evento => {
      this.evento = evento;
    });
    // creo formulario
    this.form = this.formBuilder.group({
      opciones: this.formBuilder.array([])
    });
    // pido opciones del evento
    if (!this.getFormOpciones().length) {
      this.getFormOpciones().push(this.createOpcionFormGroup())
    }
    this.opcionService.options.subscribe(opciones => {
      this.listOpciones = opciones;
      if (this.listOpciones) {
        if (this.listOpciones.length) {
          this.getFormOpciones().removeAt(0);
          opciones.forEach( (option: Option) => {
            this.getFormOpciones().push(this.createOpcionFormGroup(option))
          });
        }
      }
    });

  }

  getEvento(): Evento{
    return this.evento;
  }

  getFormOpciones(){
    return this.form.get('opciones') as FormArray
  }

  getOpciones(){
    return this.listOpciones;
    // // this.opcionService.updateDataOpcionesFromEvento(id);
  }
  
  addOpcion() {
    this.getFormOpciones().push(this.createOpcionFormGroup());
  }

  createOpcionFormGroup(data?: Option): FormGroup {
    return this.formBuilder.group({
      dia: [data?.dia, Validators.required],
      hora: [data?.hora, Validators.required],
      eventoId: [data?.eventoId || this.eventoId],
      id: [data?.id]
    });
  }

  removeOrClearOpcion(i: number) {
    const opciones = this.form.get('opciones') as FormArray
    if (opciones.length > 1) {
      const opcion = opciones.at(i).value;
      opciones.removeAt(i)
      this.opcionService.deleteOption(opcion.id);
    } else {
      opciones.reset()
    }
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    if (this.listOpciones.length){
      this.opcionService.updateOpcionesFromEvento(this.form.value.opciones);
    }else{
      this.opcionService.createOptionListFromEvent(this.form.value.opciones);
    }
  }

  onReset() {
    this.form.reset();
  }

  goBack(): void {
    this.location.back();
  }

}
