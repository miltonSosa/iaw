Estimado respecto de la corrección:


*No existe un usuario administrador que permita gestionar los usuarios y eventos. Solo existe gestión por parte de los usuarios registrados.
- se agrego usuario admin, en el inicio te lista los usuarios y en el dashboard te lista todos los eventos si sos admin o filtra si sos un usuario logeado

*Si bien permite la gestión de eventos no está claro cuando se acepta por medio del link que se comparte y tampoco se puede visualizar quiénes son los que aceptaron asistir al evento.
- esto no se entiende

*El aviso de votos de eventos solo lo pueden ver los usuarios que están registrados y no los guest. Por ejemplo, falla en visualizar el aviso cuando es guest, en todo caso no debería tener ese comportamiento con guest. Detalle en => http://hostname:4200/eleccion/1/opciones*
- se resolvio el problema, no se realizaba la votacion si no se estaba logueado, se resolvieron fix en verificacion de formulario y se agrego el nombre y apellido en los votos cuando no se esta logueado

*faltaría un reporte final de los eventos más completo donde se pueda apreciar quienes integran los meet en relación a quienes aceptaron.

*cuando se generan los eventos todos los usuarios ven todo. ¿Esto es así o faltaría algún control?. En todo caso comentanos la decisión que has tomado.
- se resolvio el punto uno, si uno esta logeado y no es admin filtra por los eventos propios nomas.

Por otro lado, es una buena iniciativa utilizar docker-compose y git en tu proyecto. Como recomendación existe documentación respecto a buenas prácticas de construcción de imágenes,por ejemplo podrías utilizar:
- en relacion a esto el docker no se penso para salida a prod sino para tener el entorno de desarrollo unificado 


FROM una imagen base => que lo tenes
RUN addgroup -S grupodetrabajo && adduser -S unuario -G grupodetrabajo => a fin de seguir la buena practica de no ejecutar como root
USER unuario:grupodetrabajo
VOLUME /pathpara exportar en caso necesario => opcional
ARG argumentos en caso de ser necesario => opcional
ADD archivos para el proyecto
ENV en caso de ser necesario variable de ambiente => opcional
ENTRYPOINT [ comando shell / run path]

aquí dejo la documentación oficial en caso que quieras explorar más:

https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
https://docs.docker.com/develop/develop-images/multistage-build/

Si tienes más información al respecto que creas necesario comentarnos por alguna razón en particular de la aplicación como funcionalidad y demás, por favor comentanos.

Se ha notado el trabajo y el desarrollo que se llevó al cabo, estarían faltando los ítems comentados de administrador y eventos por lo tanto cuando lo tengas realiza el upload y nos avisas.

saludos.



ERROR Error: Uncaught (in promise): Error: Angular JIT compilation failed: '@angular/compiler' not loaded!
  - JIT compilation is discouraged for production use-cases! Consider AOT mode instead.
  - Did you bootstrap using '@angular/platform-browser-dynamic' or '@angular/platform-server'?
  - Alternatively provide the compiler with 'import "@angular/compiler";' before bootstrapping.