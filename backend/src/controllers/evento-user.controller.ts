import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Evento,
  User,
} from '../models';
import {EventoRepository} from '../repositories';

export class EventoUserController {
  constructor(
    @repository(EventoRepository)
    public eventoRepository: EventoRepository,
  ) { }

  @get('/eventos/{id}/user', {
    responses: {
      '200': {
        description: 'User belonging to Evento',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(User)},
          },
        },
      },
    },
  })
  async getUser(
    @param.path.string('id') id: typeof Evento.prototype.id,
  ): Promise<User> {
    return this.eventoRepository.user(id);
  }
}
