import {Option, OptionRelations, Votes} from '../models';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import { VotesRepository } from './votes.repository';

export class OptionRepository extends DefaultCrudRepository<
  Option,
  typeof Option.prototype.id,
  OptionRelations
> {

  public readonly votes: HasManyRepositoryFactory<Votes, typeof Option.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource, @repository.getter('VotesRepository') protected votesRepositoryGetter: Getter<VotesRepository>,
  ) {
    super(Option, dataSource);
    this.votes = this.createHasManyRepositoryFactoryFor('votes', votesRepositoryGetter);
    this.registerInclusionResolver('votes', this.votes.inclusionResolver);
  }
}
