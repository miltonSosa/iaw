import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Suscripcion, SuscripcionRelations, User} from '../models';
import {UserRepository} from './user.repository';

export class SuscripcionRepository extends DefaultCrudRepository<
  Suscripcion,
  typeof Suscripcion.prototype.id,
  SuscripcionRelations
> {

  public readonly user: BelongsToAccessor<User, typeof Suscripcion.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Suscripcion, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
