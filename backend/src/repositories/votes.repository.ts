import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {Votes, VotesRelations, User} from '../models';
import {MongodbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {UserRepository} from './user.repository';

export class VotesRepository extends DefaultCrudRepository<
  Votes,
  typeof Votes.prototype.id,
  VotesRelations
> {

  public readonly user: BelongsToAccessor<User, typeof Votes.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Votes, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
