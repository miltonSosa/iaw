import {DefaultCrudRepository, repository, HasManyRepositoryFactory, BelongsToAccessor} from '@loopback/repository';
import {Evento, EventoRelations, Option, User} from '../models';
import {MongodbDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {OptionRepository} from './option.repository';
import {UserRepository} from './user.repository';

export class EventoRepository extends DefaultCrudRepository<
  Evento,
  typeof Evento.prototype.id,
  EventoRelations
> {

  public readonly options: HasManyRepositoryFactory<Option, typeof Evento.prototype.id>;

  public readonly user: BelongsToAccessor<User, typeof Evento.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource, @repository.getter('OptionRepository') protected optionRepositoryGetter: Getter<OptionRepository>, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Evento, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
    this.options = this.createHasManyRepositoryFactoryFor('options', optionRepositoryGetter,);
    this.registerInclusionResolver('options', this.options.inclusionResolver);
  }
}
