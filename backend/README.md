# API - IAW

Esta aplicación se genero basada en [LoopBack 4 CLI](https://loopback.io/doc/en/lb4/Command-line-interface.html). Para mas información del framework
[accede a la documentación](https://loopback.io/doc/en/lb4/index.html).

## Instalar dependencias

despues de clonar el repositorio es necesario instalar las dependencias declaradas en `package.json`, para esto ejecute el siguiente comando:

```sh
npm install
```

## Levantar la aplicación
para el entorno de desarrollo se realizo un `docker-compose.yml` en el cual se integra el entorno listo para trabajar con loopback como tambien las dependencias de servicios necesarios, despues de instalar las dependencias para levantar a aplicación y sus servicios ejecute

```sh
docker-compose up #agregar -d para levantar la aplicación en segundo plano
```


## acceder a la aplicación

abra el navegador y ingrese a la url http://localhost:3000

[![LoopBack](<https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png>)](http://loopback.io/)


{
  "username": "peron",
  "email": "peron@gmail.com",
  "emailVerified": true,
  "password": "PeronFijo45"
}

para devolver relaciones

{
  "include": [
    {
      "relation":"options"
    }
  ]
}

para devolver relaciones anidadas

{
  "include": [
    {
      "relation":"options",
      "scope": {
        "include": [{"relation":"votes"}]
      }
    }
  ]
}